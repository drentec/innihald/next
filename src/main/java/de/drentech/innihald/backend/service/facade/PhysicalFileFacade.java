package de.drentech.innihald.backend.service.facade;

import de.drentech.innihald.backend.domain.model.Document;
import de.drentech.innihald.backend.domain.model.PhysicalFile;
import de.drentech.innihald.backend.domain.repository.PhysicalFileRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class PhysicalFileFacade {

    @Inject
    private PhysicalFileRepository physicalFileRepository;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public List<PhysicalFile> getAllFiles() {
        this.logger.info("Get all physical_files");

        return this.physicalFileRepository.listAll();
    }

    public PhysicalFile getPhysicalFile(Long id) {
        this.logger.info("Get physical_file with id " + id);

        return this.physicalFileRepository.findById(id);
    }

    public PhysicalFile getByDocument(Document document) {
        Long docId = document.id;
        Long fileId = document.getFile().id;
        this.logger.info("Get physical_file for document with id " + docId);

        return this.physicalFileRepository.find("file_id", fileId).firstResult();
    }
}
