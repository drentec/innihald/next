INSERT INTO physicalfile (id, extension, filename) VALUES
    (1, 'pdf', 'Handbuch'),
    (2, 'docx', 'Bewerbungsschreiben Eva'),
    (3, 'tiff', 'Shooting-2020');


INSERT INTO document (id, title, description, file_id) VALUES
    (1, 'My first Title', 'Fixture content', 1),
    (2, 'Another document', 'Fixture content', 3),
    (3, 'Just another title', 'Fixture content', 2);
